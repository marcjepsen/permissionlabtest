package shad0w_rep.permissionlab;

import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;
import android.util.Log;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.Objects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNull.notNullValue;


@RunWith(AndroidJUnit4.class)
public class PermissionUITest {

    private static final String BASIC_SAMPLE_PACKAGE
            = "shad0w_rep.permissionlab";
    private static final int LAUNCH_TIMEOUT = 10000;
    private static final String STRING_TO_BE_TYPED = "UiAutomator";
    private UiDevice mDevice;

    @Before
    public void startMainActivityFromHomeScreen() {
        // Initialize UiDevice instance
        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        // Start from the home screen
        mDevice.pressHome();

        // Wait for launcher
        final String launcherPackage = mDevice.getLauncherPackageName();
        assertThat(launcherPackage, notNullValue());
        mDevice.wait(Until.hasObject(By.pkg(launcherPackage).depth(0)),
                LAUNCH_TIMEOUT);

        // Launch the app
        Context context = InstrumentationRegistry.getContext();
        final Intent intent = context.getPackageManager()
                .getLaunchIntentForPackage(BASIC_SAMPLE_PACKAGE);
        // Clear out any previous instances
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);

        // Wait for the app to appear
        mDevice.wait(Until.hasObject(By.pkg(BASIC_SAMPLE_PACKAGE).depth(0)),
                LAUNCH_TIMEOUT);


    }


    public void TestRunner(){}


    @Test
    public void TestContactPermissionDeny() throws UiObjectNotFoundException, InterruptedException {

        assertThat(getContactsPermissionButton().exists() && getContactsPermissionButton().click(), is(true));

        assertThat(getPermissionDenyButton().exists() && getPermissionDenyButton().click(), is(true));
    }

    @Test
    public void TestContactPermissionShowsRationalDialogDeny() throws UiObjectNotFoundException, InterruptedException {

        assertThat(getContactsPermissionButton().exists() && getContactsPermissionButton().click(), is(true));

        assertThat(getSnackbarAction().exists() && getSnackbarAction().click(), is(true));

        assertThat(getPermissionDenyButton().exists() && getPermissionDenyButton().click(), is(true));
    }

    @Test
    public void TestContactPermissionAllow() throws UiObjectNotFoundException, InterruptedException {

        assertThat(getContactsPermissionButton().exists() && getContactsPermissionButton().click(), is(true));

        assertThat(getSnackbarAction().exists() && getSnackbarAction().click(), is(true));

        assertThat(getPermissionAllowButton().exists() && getPermissionAllowButton().click(), is(true));
    }

    @Test
    public void TestContactPermissionIsAllowed() throws UiObjectNotFoundException, InterruptedException {

        assertThat(getContactsPermissionButton().exists() && getContactsPermissionButton().click(), is(true));

        assertThat(getContactListItem().exists() &&
                !getContactListItem().getText().equals(" /* hier fehlt ein String; er ist hart codiert in der MainActivity oder mit Hilfe des UiAutomator Viewer zu finden */ ") &&
                !getContactListItem().getText().equals(" /* hier fehlt ein weiterer String; er ist hart codiert in der MainActivity oder mit Hilfe des UiAutomator Viewer zu finden */ "), is(true));
    }

    public UiObject getSnackbarAction()
    {
        /*
            ResourceId und className können über UiAutomator ermittelt werden
        */
        return mDevice.findObject(new UiSelector()
                .resourceId(" /* hier fehlt eine resource-id, der UiAutomator Viewer hilft hier weiter */ ")
                .className(" /* hier fehlt der korrekte class-Name, der UiAutomator Viewer hilft hier weiter */ "));
    }

    public UiObject getContactsPermissionButton()
    {
        /*
            ResourceId und className können über UiAutomator ermittelt werden
        */
        return mDevice.findObject(new UiSelector()
                .resourceId(" /* hier fehlt eine resource-id, der UiAutomator Viewer hilft hier weiter */ ")
                .className(" /* hier fehlt der korrekte class-Name, der UiAutomator Viewer hilft hier weiter */ "));
    }

    public UiObject getPermissionAllowButton()
    {
        /*
            ResourceId und className können über UiAutomator ermittelt werden
        */
        return mDevice.findObject(new UiSelector()
                .resourceId(" /* hier fehlt eine resource-id, der UiAutomator Viewer hilft hier weiter */ ")
                .className(" /* hier fehlt der korrekte class-Name, der UiAutomator Viewer hilft hier weiter */ "));
    }

    public UiObject getPermissionDenyButton()
    {
        /*
            ResourceId und className können über UiAutomator ermittelt werden
        */
        return mDevice.findObject(new UiSelector()
                .resourceId(" /* hier fehlt eine resource-id, der UiAutomator Viewer hilft hier weiter */ ")
                .className(" /* hier fehlt der korrekte class-Name, der UiAutomator Viewer hilft hier weiter */ "));
    }

    public UiObject getContactListItem()
    {
        /*
            ResourceId und className können über UiAutomator ermittelt werden
        */
        return mDevice.findObject(new UiSelector()
                .resourceId("android:id/text1")
                .className("android.widget.TextView"));
    }



}
