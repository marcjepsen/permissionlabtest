package shad0w_rep.permissionlab;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.support.design.widget.Snackbar;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "Main Activity";

    private final int PICK_CONTACT = 1;

    private final int REQUEST_PERMISSION_READ_CONTACTS = 1;

    public ProgressDialog loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setListViewItem("No Item selected!");
    }

    public void setListViewItem(String item) {
        ListView listview = (ListView) findViewById(R.id.listView);
        ArrayList<String> list = new ArrayList<String>();
        list.add(item);

        ArrayAdapter<String> listAdapter= new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list);

        listview.setAdapter(listAdapter);
    }

    /**
     * Permission Part
     */
    public void contactsPermissionsButtonClicked(View view) {
        Log.i(TAG, "Contacts Permission button clicked!");
        setListViewItem("No Item selected!");
        // Andorid < 6.0
//         setContactPermission();

        //  Android >= 6.0
        if(verifyPermissions(Manifest.permission.READ_CONTACTS)) {
            Log.i(TAG, "Permission verified");
            setContactPermission();
        } else {
            Log.i(TAG, "Requesting Permission!");
            requestContactsPermission();
        }

    }

    public void setContactPermission() {
        Uri uri = ContactsContract.Contacts.CONTENT_URI;
        getContactByUri(uri);
    }

    private void requestContactsPermission() {
        // Permission erklaeren ?!
        // Der Benutzer hat vorher erstmal die Berechtigung abgelehnt...
        if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                Manifest.permission.READ_CONTACTS)) {

            // Wir erklären kurz den grund der Permission
            Log.i(TAG, "Explain!");
            View layout = findViewById(R.id.main_layout);
            Snackbar.make(layout, "Dies ist der Grund für READ CONTACTS PERMISSION",
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction("Ok", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    new String[]{Manifest.permission.READ_CONTACTS},
                                    REQUEST_PERMISSION_READ_CONTACTS);
                        }
                    })
                    .show();

        } else {

            // Es wird keine erläutung benoetigt
            Log.i(TAG, "Do requestPermissions");
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.READ_CONTACTS},
                    REQUEST_PERMISSION_READ_CONTACTS);
        }
    }
    private boolean verifyPermissions(String permission) {
        if( ContextCompat.checkSelfPermission(MainActivity.this, permission)== PackageManager.PERMISSION_GRANTED) {
            return true;
        }

        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // Permission gegeben, führe aufgabe aus.
                    setContactPermission();

                } else {

                    // permission abgeleht, funktionalitaet einschraenken
                    setListViewItem("READ CONTACTS Permission wurde nicht erteilt! :(");
                }
                return;
            }

            // Hier können andere Permission faelle berarbeitet werden
        }
    }

    /**
     * Intent Part !
     */
    public void contactsIntentButtonClicked(View view) {

        Log.i(TAG, "Contact Intent Button clicked!");
        setListViewItem("No Item selected!");

        // Contacts Best Practice!
        // Kontakte über einen Intent beziehen
        // Hier werden keine Permissions benötigt
        contactsIntent();
    }


    public void contactsIntent() {
        loading = ProgressDialog.show(MainActivity.this, "Loading", "Kontakte werden geladen ... ");
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);

        startActivityForResult(intent, PICK_CONTACT);
    }

    public void getContactByUri(Uri uri) {
        ContentResolver contentResolver = getContentResolver();
        String[] projection = {
                ContactsContract.Contacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME
        };

        Cursor cursor = contentResolver.query(uri, projection, null, null, null);

        if( cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                Log.i(TAG, "Contact picked: " + name);
                setListViewItem(name);
            }
            cursor.close();
        }
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        loading.hide();

        switch (reqCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    getContactByUri(data.getData());
                }
                break;
        }
    }
}
